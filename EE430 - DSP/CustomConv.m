close all;
fs = 30;
fc = 7;
h = fir1(10, fc/(fs/2));
% Input
f = 10;
f2 = 5;
T = 1/fs;
n = 0:4*fs;
k = n*T;
x = randn(1, length(k))/5 + cos(2*pi*f.*k) + cos(2*pi*f2.*k);

% y = symsum(h(m)*x(n-m), m, n);
y = zeros(1, length(x));

for n = 1:length(x)
  for m = 1:length(h)
    if (n-m) > 1 && (n-m) < length(x)
      y(n) += h(m)*x(n-m);
    else
      y(n) += 0;
    endif
  end
end

figure;
subplot(2, 2, 1);
plot(x);
title("Input signal");
subplot(2, 2, 2);
plot(y);
title("Output signal");
subplot(2, 2, 3);
X = abs(fft(x, 2^8));
f = (1:length(X))*fs/length(X);
plot(f(1:end/2),X(1:length(X)/2)); box off; axis tight;
title("FFT of Input");
subplot(2, 2, 4);
X = abs(fft(y, 2^8));
f = (1:length(X))*fs/length(X);
plot(f(1:end/2),X(1:length(X)/2)); box off; axis tight;
title("FFT of Output");