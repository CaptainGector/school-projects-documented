clear;
close all;
pkg load signal;

function [Ind, Samp] = PressureDetect(xi,fs=125,pf=0)
  % Setup/segment variables
  y = xi;
  t_elap = 5;
  t_start = 1000;
  t = round(linspace(t_start*fs, (t_start+t_elap)*fs, t_elap*fs));
    
  % Filter out the quantization noise on the signal (lowpass)
  % It looks like the highest significant frequency content could be reasonably
  % cutoff below 15Hz, so we'll use that.
  [B, A] = butter(5, 15/(fs/2));
  y_noerr = filter(B, A, y);
  
  plot_fig = 0;
  if pf==1;
    plot_fig = figure("Name", "Plot of Peaks");
    plot(linspace(t_start, t_start+t_elap, length(y_noerr(t))), y_noerr(t));
    hold on;
  endif

  % Compare with a low-pass filtered signal.
  [B, A] = butter(10, 2/(fs/2));
  y_low = filter(B, A, y_noerr);
  if pf==1;
    plot(linspace(t_start, t_start+t_elap, t_elap*fs), y_low(t), ":");
  endif
  

  T_s = 1/2;
  n_forward = round(T_s*fs);
  % We want to start looking after the dip in the signal, which should occur 
  % about 1/2 wavelength after the peak.
  n_dip = round(T_s/2*fs);
  low_peaks(1) = find(y_low == max(y_low(1:n_forward)));
  i = 2;
  while (low_peaks(i-1)+n_forward+n_dip) < length(y_low);
    y_range = y_low(low_peaks(i-1)+n_dip:low_peaks(i-1)+n_forward+n_dip);
    low_peaks(i) = low_peaks(i-1)+ n_dip + find(y_range == max(y_range));
    i++;
  endwhile

  % Plot the peaks of the low-pass filtered signal
  if pf==1;
    set(0,'CurrentFigure', plot_fig)
    points = low_peaks;
    m1 = interp1(points,1:length(points), t(1), "nearest");
    m2 = interp1(points,1:length(points), t(end), "nearest");
    y_marks = y_low(points(m1:m2)-1);
    plot(points(m1:m2)/fs, y_marks, "r*");
    xlim([t_start (t_start+t_elap)]);
  endif
  
  % Okay, now, here's the fun part. Look at a period of time before and after 
  % the low-pass filter peaks. 
  % The period should be under the period of the lowpass signal. 
  % For now, we'll just use a static value.
  T_s = 1/2;
  N_diff = round((T_s/2)*fs);
  high_peaks = 0;
  i = 1;
  while i < length(low_peaks);
    y_range = y_noerr((low_peaks(i)-(N_diff)):(low_peaks(i)+(N_diff)));
    high_peaks(i) = low_peaks(i)-N_diff + find(y_range == max(y_range))(1);
    i++;
  endwhile
  if pf==1;
    set(0,'CurrentFigure', plot_fig)
    points = high_peaks;
    peaked = y_noerr(points);
    m1 = interp1(points,1:length(points), t(1), "nearest");
    m2 = interp1(points,1:length(points), t(end), "nearest");
    y_marks = y_noerr(points(m1:m2)-1);
    plot((points(m1:m2)-1)/fs, y_marks, "b*");
    xlim([t_start (t_start+t_elap)]);
    xlabel("Time (s)"); ylabel("Amplitude");
  endif
  
  Ind = high_peaks;
  Samp = xi(Ind);
endfunction