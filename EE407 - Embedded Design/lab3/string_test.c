/*************************************************************************
Simple Char Array "String" Test in C
Allan Douglas
Oregon Tech, 2017
*************************************************************************/

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

int main () {

   char str1[12] = "Hello";
   char str2[12] = "World";
   char str3[12];
   int  len ;
   int i;
   char *ptr;

   printf("------------------------------------------------------------\n");
   printf("Check the size of different data types in bytes.\n");
   printf("------------------------------------------------------------\n");

   printf("character size :          %d byte(s)\n", (int)sizeof(char));
   printf("character pointer size :  %d byte(s)\n", (int)sizeof(char*));
   printf("integer size :            %d byte(s)\n", (int)sizeof(int));
   printf("integer pointer size :    %d byte(s)\n", (int)sizeof(int*));
   printf("long size :               %d byte(s)\n", (int)sizeof(long));
   printf("long pointer size :       %d byte(s)\n", (int)sizeof(long*));

   printf("------------------------------------------------------------\n");
   printf("Check the initial values of the char arrays.\n");
   printf("------------------------------------------------------------\n");
   printf("str1 :  %s\n", str1 );
   printf("str2 :  %s\n", str2 );
   printf("str3 :  %s\n", str3 );

   len = strlen(str1);
   ptr = str1;
   printf("addr(str1) :  0x%lx\n", (unsigned long)ptr);

   for(i=0;i<=len;i++) {
      printf("str1 addr 0x%lx :  0x%02x  :  %c\n", (unsigned long)(ptr+i), (unsigned int)(*(ptr+ i)), *(ptr+ i));
   }

   len = strlen(str2);
   ptr = str2;
   printf("addr(str2) :  0x%lx\n", (unsigned long)ptr);

   for(i=0;i<=len;i++) {
      printf("str2 addr 0x%lx :  0x%02x  :  %c\n", (unsigned long)(ptr+i), (unsigned int)(*(ptr+ i)), *(ptr+ i));
   }

   len = strlen(str3);
   ptr = str3;
   printf("addr(str3) :  0x%lx\n", (unsigned long)ptr);

   for(i=0;i<=len;i++) {
      printf("str3 addr 0x%lx :  0x%02x  :  %c\n", (unsigned long)(ptr+i), (unsigned int)(*(ptr+ i)), *(ptr+ i));
   }

   printf("------------------------------------------------------------\n");
   printf("Modify the char arrays.\n");
   printf("------------------------------------------------------------\n");

   /* copy str1 into str3 */
   strcpy(str3, str1);
   printf("strcpy( str3, str1) :  %s\n", str3 );

   /* concatenates str1 and str2 */
   strcat( str1, str2);
   printf("strcat( str1, str2):   %s\n", str1 );

  /* zero str2 */
   bzero(str2, sizeof(str2));
   printf("bzero(str2, sizeof(str2));   %s\n", str2 );

  /* fill str3 with the char "A" */
   memset((char *)&str3, 65, 10);
   printf("memset((char *)&str3, 65, 10);   %s\n", str3 );

   printf("------------------------------------------------------------\n");
   printf("Check the new values of the char arrays\n");
   printf("------------------------------------------------------------\n");

   len = strlen(str1);
   ptr = str1;
   printf("addr(str1) :  0x%lx\n", (unsigned long)ptr);

   for(i=0;i<=len;i++) {
      printf("str1 addr 0x%lx :  0x%02x  :  %c\n", (unsigned long)(ptr+i) , (unsigned int)(*(ptr+ i)), *(ptr+ i));
   }

   len = strlen(str2);
   ptr = str2;
   printf("addr(str2) :  0x%lx\n", (unsigned long)ptr);

   for(i=0;i<=len;i++) {
      printf("str2 addr 0x%lx :  0x%02x  :  %c\n", (unsigned long)(ptr+i), (unsigned int)(*(ptr+ i)), *(ptr+ i));
   }

   len = strlen(str3);
   ptr = str3;
   printf("addr(str3) :  0x%lx\n", (unsigned long)ptr);

   for(i=0;i<=len;i++) {
      printf("str3 addr 0x%lx :  0x%02x  :  %c\n", (unsigned long)(ptr+i), (unsigned int)(*(ptr+ i)), *(ptr+ i));
   }

   return 0;
}
